angular.module('angularRestfulAuth')
    .controller('cadastroColaboradoresCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterColaboradores = function() {
            var formData = {
                id : 123,
                id_cond: 456
            }
            Main.obterColaboradores(formData, function(res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.salvarColaborador = function() {
            var formData = {
                id: 021456,
                nome_colaborador: 'Jose da Silva',
                cpf : '01524709131',
                rg: '01524709131',
                id_cargo: 654,
                id_cond: 123,
                telefone: '62984361583'
                //trocar pelos dados vindo do formulario (params Form)
            }
            Main.salvarColaborador(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };
            
    }]);
