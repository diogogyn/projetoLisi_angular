angular.module('angularRestfulAuth').controller('finalizarReceitaCtrl', function($scope) {
	$scope.indexMessage = 'oi eu estou adicionando a navegação para o site!!!'

	var buscarPacientes = function(nomePaciente) {
		// aqui vai o codigo do request vindo do controler
		$scope.pacientesBusca = [ {
			id : 1,
			nome : "Paciente 1",
			dtNascimento : "00/00/0000",
			nomeMae : "Nila di Tal"
		}, {
			id : 2,
			nome : "Paciente 2",
			dtNascimento : "00/00/0000",
			nomeMae : "Nilson di Tal"
		}, {
			id : 3,
			nome : "Paciente 3",
			dtNascimento : "00/00/0000",
			nomeMae : "Ana Teresa"
		} ];
	};

	var carregarItensDeReceita = function(idReceita) {
		$scope.itensReceita = [ {
			cod_medicamento : 0000,
			medicamento : {
				codigo_medicamento : 0004,
				nome_medicamento : "Medicamento 1",
				unidSaida : "CT"
			},
			intervalo : 1,
			qtdePrescrita : 1,
			tempoTratamento : 1,
			qtde_dispensar : 10,
			prescricao : 1 + " " + "CX" + " p/ " + 1 + " dia(s), por " + 1
					+ " dia(s)"
		} ];
	};
	// buscar as receitas prescritas para um determinado paciente
	var carregarReceitasPrescritas = function(idPaciente) {
		$scope.receitas_prescritas = [ {
			idReceita : 1,
			numReceita : "1000-000-000",
			dtEmissao : "00/00/0000",
			status : "aberta",
			qtdeTotal : 10,
			profissional : "Diogo Oliveira"
			
		}, {
			idReceita : 1,
			numReceita : "2000-000-000",
			dtEmissao : "00/00/0000",
			status : "aberta",
			qtdeTotal : 10,
			profissional : "Ivana Oliveira"			
		}, {
			idReceita : 1,
			numReceita : "3000-000-000",
			dtEmissao : "00/00/0000",
			status : "aberta",
			qtdeTotal : 10,
			profissional : "Roseni Rodrigues Oliveira"
		}, {
			idReceita : 1,
			numReceita : "1230-000-000",
			dtEmissao : "00/00/0000",
			status : "aberta",
			qtdeTotal : 10,
			profissional : "Diogo Oliveira"
		} ];
	};
	var carregarItensDeReceita = function(receita) {
		$scope.itensReceita = [ {
			medicamento : {
				codigo_medicamento : 0004,
				nome_medicamento : "Medicamento 1",
				unidSaida : "CT"
			},
			qtdeTotal : 10,
			enderecoItem : {
				area : 1,
				modulo : 2,
				nivel : 1,
				rua : 1,
				vao : 1
			}
		}, {
			medicamento : {
				codigo_medicamento : 0004,
				nome_medicamento : "Medicamento 3",
				unidSaida : "CT"
			},
			qtdeTotal : 10,
			enderecoItem : {
				area : 1,
				modulo : 1,
				nivel : 1,
				rua : 6,
				vao : 4
			}
		}, {
			medicamento : {
				codigo_medicamento : 0004,
				nome_medicamento : "Medicamento 5",
				unidSaida : "CT"
			},
			qtdeTotal : 10,
			enderecoItem : {
				area : 1,
				modulo : 1,
				nivel : 1,
				rua : 3,
				vao : 6
			}
		} ];
		$scope.receitaSelecionada = receita.numReceita;
	};
	// funcao para o form para pesquisa de um paciente
	$scope.pesquisarPaciente = function(paciente) {
		buscarPacientes(paciente);
		$scope.itensReceita = [];
		$scope.receitas_prescritas = [];
	};

	$scope.pesquisarReceitasAbertas = function(paciente) {
		carregarReceitasPrescritas(paciente);
	};

	$scope.buscarItensReceita = function(receita) {
		carregarItensDeReceita(receita);
	};
	/*
	$scope.isItemSelecionado = function(itens) {
		return itens.some(function (item) {
			return item.selecionado;
		});
	};*/
})