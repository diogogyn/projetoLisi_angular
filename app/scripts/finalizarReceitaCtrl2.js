app.controller('finalizarReceitaCtrl2', function($scope) {
	$scope.minhaMessagem = 'oi eu estou adicionando a navegação para o site!!!'

	var buscarPacientes = function(nomePaciente) {
		// aqui vai o codigo do request vindo do controler
		$scope.pacientesBusca = [ {
			id : 1,
			nome : "Paciente 1",
			dtNascimento : "00/00/0000",
			nomeMae : "Fulaninha di Tal"
		}, {
			id : 2,
			nome : "Paciente 2",
			dtNascimento : "00/00/0000",
			nomeMae : "Ciclana di Tal"
		}, {
			id : 3,
			nome : "Paciente 3",
			dtNascimento : "00/00/0000",
			nomeMae : "Beltrano di Tal"
		} ];
	};

	var carregarItensDeReceita = function(idReceita) {
		$scope.itensReceita = [ {
			cod_medicamento : 0000,
			medicamento : {
				codigo_medicamento : 0004,
				nome_medicamento : "Medicamento 1",
				unidSaida : "CT"
			},
			intervalo : 1,
			qtdePrescrita : 1,
			tempoTratamento : 1,
			qtde_dispensar : 10,
			prescricao : 1 + " " + "CX" + " p/ " + 1 + " dia(s), por " + 1
					+ " dia(s)"
		} ];
	};
	// buscar as receitas prescritas para um determinado paciente
	var carregarReceitasPrescritas = function(idPaciente) {
		$scope.receitas_prescritas = [ {
			idReceita : 1,
			numReceita : "0000-000-000",
			dtEmissao : "00/00/0000",
			status : aberta,
			profissional : "alguem ai",
			qtdeTotal : 10
		}, {
			idReceita : 1,
			numReceita : "0000-000-000",
			dtEmissao : "00/00/0000",
			status : aberta,
			profissional : "alguem ai",
			qtdeTotal : 10
		}, {
			idReceita : 1,
			numReceita : "0000-000-000",
			dtEmissao : "00/00/0000",
			status : aberta,
			profissional : "alguem ai",
			qtdeTotal : 10
		}, {
			idReceita : 1,
			numReceita : "0000-000-000",
			dtEmissao : "00/00/0000",
			status : aberta,
			profissional : "alguem ai",
			qtdeTotal : 10
		} ];
	};
	var carregarItensDeReceita = function(receita) {
		$scope.itensReceita = [ {
			cod_medicamento : 0078,
			medicamento : {
				codigo_medicamento : 0004,
				nome_medicamento : "Medicamento 1",
				unidSaida : "CT"
			},
			qtdeTotal : 10,
			enderecoItem : {
				area : 1,
				modulo : 1,
				nivel : 1,
				rua : 1,
				vao : 1
			}
		}, {
			cod_medicamento : 0340,
			medicamento : {
				codigo_medicamento : 0004,
				nome_medicamento : "Medicamento 1",
				unidSaida : "CT"
			},
			qtdeTotal : 10,
			enderecoItem : {
				area : 1,
				modulo : 1,
				nivel : 1,
				rua : 1,
				vao : 1
			}
		}, {
			cod_medicamento : 0020,
			medicamento : {
				codigo_medicamento : 0004,
				nome_medicamento : "Medicamento 1",
				unidSaida : "CT"
			},
			qtdeTotal : 10,
			enderecoItem : {
				area : 1,
				modulo : 1,
				nivel : 1,
				rua : 1,
				vao : 1
			}
		} ];
		$scope.receitaSelecionada = receita.numReceita;
	};
	// funcao para o form para pesquisa de um paciente
	$scope.pesquisarPaciente = function(paciente) {
		buscarPacientes(paciente);
		$scope.itensReceita = [];
		$scope.receitas_prescritas = [];
	};

	$scope.pesquisarReceitasPrescritas = function(paciente) {
		carregarReceitasPrescritas(paciente);
	};

	$scope.buscarItensReceita = function(idReceita) {
		carregarItensDeReceita(idReceita);
	};

})