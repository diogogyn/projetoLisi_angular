
angular.module('angularRestfulAuth')
    .controller('cadastroLocalidadesCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function ($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterLocalidades = function () {
            var formData = {
                id : 123,
                id_cond: 456
            };
            Main.obterLocalidades(formData, function (res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to load';
            });
        };

        $scope.salvarLocalidade = function () {
            var formData = {
                id: 021456,
                nome_fornecedor: 'Sync Consultoria',
                cpf_cnpj: '01524709131',
                id_ramo: 654,
                telefone: '62984361583'
                //trocar pelos dados vindo do formulario (params Form)
            };
            Main.salvarLocalidade(formData, function (res) {
                if (res.data.success === true) {
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to save';
            });
        };
            
    }]);
