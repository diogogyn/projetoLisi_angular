
angular.module("angularRestfulAuth").config(function($routeProvider){
	
	console.log("passou por routConfig lá!!!");

    $routeProvider.
        when("/", {
            templateUrl: "/partials/home/home.html",
            controller: "indexCtrl"
        }).
        when("/index", {
            templateUrl: "partials/home/home.html",
            controller: "indexCtrl"
        }).
        when("/login", {
            templateUrl: "partials/login/login.html",
            controller: "loginCtrl"
        })        
        .when("/error", {
            templateUrl: "partials/erros/e404.html",
            controller: "indexCtrl"
        })
        .when("/avisos", {
            templateUrl: "partials/avisos/avisos.html",
            controller: "avisosCtrl",
            authorize: true
        })
        .when("/documentos", {
            templateUrl: "partials/documentos/documentos.html",
            controller: "documentosCtrl",
            authorize: true
        })
        .when("/mudancas", {
            templateUrl: "partials/mudancas/mudancas.html",
            controller: "mudancasCtrl",
            authorize: true
        })
        .when("/ocorrencias", {
            templateUrl: "partials/ocorrencias/ocorrencias.html",
            controller: "ocorrenciasCtrl",
            authorize: true
        })
        .when("/reservas", {
            templateUrl: "partials/reservas/reservas.html",
            controller: "reservasCtrl",
            authorize: true
        })
        .when("/cadastroProfissional",{
            templateUrl: "partials/cadastro/cadastroProfissional.html",
            controller: "novoProfissionalCtrl"
        })
        .when("/listaProfissionais",{
            templateUrl: "partials/profissional/listaProfissionais.html",
            controller: "listaProfissionaisCtrl"
        })
        .when("/cadastroPaciente",{
            templateUrl: "partials/cadastro/cadastroPaciente.html",
            controller: "novoPacienteCtrl"
        })
        .when("/cadastroReceita",{
            templateUrl: "partials/receita/cadastroReceita.html",
            controller: "novaReceitaCtrl"
        })
        .when("/finalizarReceita",{
            templateUrl: "partials/receita/finalizarReceita.html",
            controller: "finalizarReceitaCtrl"
        })
        .when("/entradaMaterial",{
            templateUrl: "partials/material/entradaMaterial.html",
            controller: "entradaMaterialCtrl"
        })
        .when("/middleware",{
            templateUrl: "partials/rfid/middleware.html",
            controller: "middlewareCtrl"
        })
        .when("/medicamentosPorProfissional",{
            templateUrl: "partials/relatorios/medicamentosPorProfissional.html",
            controller: "medicamentosPorProfissionalCtrl"
        })
        .when("/comprovanteRetirada",{
            templateUrl: "partials/relatorios/comprovanteRetirada.html",
            controller: "comprovanteRetiradaCtrl"
        })
        .when("/estoqueGeral",{
            templateUrl: "partials/relatorios/estoqueGeral.html",
            controller: "estoqueGeralCtrl"
        })      
        .when("/medicamentosAVencer",{
            templateUrl: "partials/relatorios/medicamentosAVencer.html",
            controller: "medicamentosAVencerCtrl"
        })
        .when("/medicamentosPorPaciente",{
            templateUrl: "partials/relatorios/medicamentosPorPaciente.html",
            controller: "medicamentosPorPacienteCtrl"
        })
        .when("/listaRastreamentos",{
            templateUrl: "partials/rastreamentos/listaRastreamentos.html",
            controller: "listaRastreamentosCtrl"
        })
        .when("/detalhaRastreamento",{
            templateUrl: "partials/rastreamentos/detalhaRastreamento.html",
            controller: "listaRastreamentosCtrl"
        })
        .otherwise({
            redirectTo: "/"
        });

})