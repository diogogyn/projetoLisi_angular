
angular.module('angularRestfulAuth')
    .controller('ocorrenciasCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterOcorrencias = function() {
            var formData = {
                id : 123,
                id_cond: 456
            }
            Main.obterOcorrencias(formData, function(res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.salvarOcorrencia = function() {
            var formData = {
                id: 021456,
                id_morador: 132,
                apartamento : 456,
                bloco: Pacari,
                titulo: 'Piscina suja',
                status:  'em analise ou em execucao ou fechada',
                prioridade: 'baixa ou media ou alta',
                id_responsavel: 987,
                descricao: 'Piscina suja a muito tempo',
                mensagem: 'Já está sendo verificado',
                image: ''
                //trocar pelos dados vindo do formulario (params Form)
            }
            Main.salvarOcorrencia(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };

        $scope.agruparOcorrencias = function() {
            var formData = {
                id_master : 123,
                id_minnor: 456
            }
            Main.agruparOcorrencias(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados agrupados';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.salvarMensagemOcorrencia = function() {
            var formData = {
                id_ocorrencia : 123,
                mensagem: 'Estamos verificando'
            }
            Main.salvarMensagemOcorrencia(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };
            
    }]);
