/* Controllers */

angular.module('angularRestfulAuth')
    .controller('avisosCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterAvisos = function() {
            var formData = {
                inicio : '01/01/2017',
                fim: '11/03/2017',
                id: 123,
                id_cond: 456
            }
            Main.obterAvisos(formData, function(res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.salvarAviso = function() {
            var formData = {
                id: 021456,
                id_colaborador_resp: 132,
                id_cond: 987,
                titulo: 'Piscina suja',
                data_aviso:  '10/03/2017 10:10:15'
                //trocar pelos dados vindo do formulario (params Form)
            }
            Main.salvarAviso(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };

    }]);
