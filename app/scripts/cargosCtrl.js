
angular.module('angularRestfulAuth')
    .controller('cargosCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterCargos = function () {
            var formData = {
                id : 123
            };
            Main.obterCargos(formData, function (res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to load';
            });
        };

        $scope.salvarCargo = function () {
            var formData = {
                id: 123,
                nome : 'Gerente'
                //trocar pelos dados vindo do formulario (params Form)
            };
            Main.salvarCargo(formData, function (res) {
                if (res.data.success === true) {
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to save';
            });
        };

    }]);
