<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Comprovante de retirada de Medicamentos</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" ng-controller="comprovanteRetiradaCtrl">
	<div class="col-lg-12">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Paciente: Fulano di Tal</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<form role="form">
							<div class="col-lg-2">
								<div class="form-group">
									<label>Nº da receita:</label>
									<p>0000-000-000M</p>									
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label>Data da Emissão:</label>
									<p>00/00/0000</p>									
								</div>
							</div>
							<div class="col-lg-3">
								<div class="form-group">
									<label>Pescritor:</label>
									<p>Dr FrankEinstein</p>									
								</div>
							</div>
							<div class="col-lg-3">
								<br>
								<div class="form-group">
									<button type="button" class="btn btn-primary" ng-click="imprimirReceita(paciente)">Imprimir</button>
								</div>
							</div>
							<div class="col-lg-3">
								<div class="form-group">
									<label>QTDE de Itens:</label>
									<p>25 Itens</p>									
								</div>
							</div>
						</form>
					</div>
					
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Itens da receita</h4>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel-body">
							<div class="dataTable_wrapper">
								
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-itensReceita">
									<thead>
										<tr>
											<th>CÓDIGO</th>
											<th>MATERIAL</th>
											<th>QTDE SOLIC.</th>
											<th>LOCAL ARMAZ.</th>
											<th><p class="btn btn-outline btn-warning btn-xs">OK?</p></th>
										</tr>
									</thead>
									<tbody>
										<tr class="gradeA" ng-repeat="item in itensReceita">
											<td>{{item.medicamento.codigo_medicamento}}</td>
											<td>{{item.medicamento.nome_medicamento}}</td>
											<td>{{item.qtdeTotal}} {{item.medicamento.unidSaida}}</td>
											<td class="center">
												<p class="btn btn-outline btn-info btn-xs">A: {{item.enderecoItem.area}}</p>
												<p class="btn btn-outline btn-primary btn-xs">R: {{item.enderecoItem.rua}}</p>
												<p class="btn btn-outline btn-danger btn-xs">M: {{item.enderecoItem.modulo}}</p>
												<p class="btn btn-outline btn-default btn-xs">N: {{item.enderecoItem.nivel}}</p>
												<p class="btn btn-outline btn-warning btn-xs">V: {{item.enderecoItem.vao}}</p>
											</td>
											<td><input readonly type="checkbox" ng-model="item.selecionado" /></td>
										</tr>
									</tbody>
								</table>
								<!-- /.table-responsive -->
							</div>
							<!-- /.dataTable_wrapper -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
	<script>
		$(document).ready(function() {
			$('#dataTables-buscaPacientes').DataTable({
				responsive : true
			});
		});
		$(document).ready(function() {
			$('#dataTables-itensReceita').DataTable({
				responsive : true
			});
		});
		$(document).ready(function() {
			$('#dataTables-receitasPrescritas').DataTable({
				responsive : true
			});
		});
	</script>
</div>
