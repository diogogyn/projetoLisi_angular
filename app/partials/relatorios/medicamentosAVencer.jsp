<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Relatório de Medicamentos Vencidos e à
			Vencer</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" ng-controller="medicamentosAVencerCtrl">
	<div class="col-lg-12">
		<div class="panel-group" id="accordion">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">BUSCA</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Medicamento</label> <select class="form-control" name="">
									<option value="">TODOS</option>
									<option value="">MEDICAMENTO 1</option>
									<option value="">MEDICAMENTO 2</option>
									<option value="">MEDICAMENTO com nome comprido demais</option>
								</select>
							</div>
						</div>
						<!-- col-lg-3 -->
						<div class="col-lg-3">
							<div class="form-group">
								<label>Data inicial</label>
								<inputt class="form-control" type="text" name="nome" />
							</div>
						</div>
						<!-- col-lg-3 -->
						<div class="col-lg-3">
							<div class="form-group">
								<label>Data final</label>
								<inputt class="form-control" type="text" name="nome" />
							</div>
						</div>
						<!-- col-lg-3 -->
						<div class="col-lg-2">
							<div class="form-group">
								<br>
								<button class="btn btn-primary" ng-click="">Buscar Histórico</button>
							</div>
						</div>
						<!-- col-lg-3 -->
					</div>
					<!-- row -->

				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Resumo</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-4">
							<p>Periodo: 00/00/0000 à 00/00/0000</p>
						</div>
						<!-- col-lg-4 -->
						<div class="col-lg-4">
							<p>Qtde de Medicamentos a vencer: 150 Itens</p>
						</div>
						<!-- col-lg-4 -->
						<div class="col-lg-4">
							<p>Qtde de Medicamentos a vencidos: 50 Itens</p>
						</div>
						<!-- col-lg-4 -->
					</div>
					<!-- row -->

				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Medicamentos encontrados</h4>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-itensReceita">
									<thead>
										<tr>
											<th>CÓDIGO</th>
											<th>MEDICAMENTO/MATERIAL.</th>
											<th>QTDE Á VENCER</th>
											<th>QTDE VENCIDO</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr class="gradeA">
											<td>0000</td>
											<td>Dipirona Sodica 100 mg</td>
											<td>10</td>
											<td>2</td>
											<td><button ng-click=""
													class="btn btn-ouside btn-primary btn-xs">VISUALIZAR</button></td>
										</tr>
										<tr class="gradeA">
											<td>0000</td>
											<td>Dipirona Sodica 100 mg</td>
											<td>10</td>
											<td>2</td>
											<td><button ng-click=""
													class="btn btn-ouside btn-primary btn-xs">VISUALIZAR</button></td>
										</tr>
										<tr class="gradeA">
											<td>0000</td>
											<td>Dipirona Sodica 100 mg</td>
											<td>10</td>
											<td>2</td>
											<td><button ng-click=""
													class="btn btn-ouside btn-primary btn-xs">VISUALIZAR</button></td>
										</tr>
									</tbody>
								</table>
								<!-- /.table-responsive -->
							</div>
							<!-- /.dataTable_wrapper -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">DETALHES : Dipirona Sodica 100 mg</h4>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="dataTables-itens">
									<thead>
										<tr>
											<th>CÓDIGO RFID</th>
											<th>DT. ENTRADA</th>
											<th>DT. VALIDADE</th>
											<th>SITUAÇÃO</th>
											<th>LOCAL</th>
										</tr>
									</thead>
									<tbody>
										<tr class="gradeA">
											<td>0000</td>
											<td>00/00/0000</td>
											<td>00/00/0000</td>
											<td><p ng-click=""
													class="btn btn-ouside btn-warning btn-xs">A VENCER</p></td>
											<td class="center">
												<p class="btn btn-outline btn-info btn-xs">A: 1</p>
												<p class="btn btn-outline btn-primary btn-xs">R: 1</p>
												<p class="btn btn-outline btn-danger btn-xs">M: 1</p>
												<p class="btn btn-outline btn-default btn-xs">N: 1</p>
												<p class="btn btn-outline btn-warning btn-xs">V: 1</p>
											</td>
										</tr>
										<tr class="gradeA">
											<td>0000</td>
											<td>00/00/0000</td>
											<td>00/00/0000</td>
											<td><p ng-click=""
													class="btn btn-ouside btn-danger btn-xs">VENCIDO</p></td>
											<td class="center">
												<p class="btn btn-outline btn-info btn-xs">A: 1</p>
												<p class="btn btn-outline btn-primary btn-xs">R: 1</p>
												<p class="btn btn-outline btn-danger btn-xs">M: 1</p>
												<p class="btn btn-outline btn-default btn-xs">N: 1</p>
												<p class="btn btn-outline btn-warning btn-xs">V: 1</p>
											</td>
										</tr>
										<tr class="gradeA">
											<td>0000</td>
											<td>00/00/0000</td>
											<td>00/00/0000</td>
											<td><p ng-click=""
													class="btn btn-ouside btn-warning btn-xs">A VENCER</p></td>
											<td class="center">
												<p class="btn btn-outline btn-info btn-xs">A: 1</p>
												<p class="btn btn-outline btn-primary btn-xs">R: 1</p>
												<p class="btn btn-outline btn-danger btn-xs">M: 1</p>
												<p class="btn btn-outline btn-default btn-xs">N: 1</p>
												<p class="btn btn-outline btn-warning btn-xs">V: 1</p>
											</td>
										</tr>
									</tbody>
								</table>
								<!-- /.table-responsive -->
							</div>
							<!-- /.dataTable_wrapper -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
	<script>
		$(document).ready(function() {
			$('#dataTables-buscaPacientes').DataTable({
				responsive : true
			});
		});
		$(document).ready(function() {
			$('#dataTables-itens').DataTable({
				responsive : true
			});
		});
		$(document).ready(function() {
			$('#dataTables-receitasPrescritas').DataTable({
				responsive : true
			});
		});
	</script>
</div>
