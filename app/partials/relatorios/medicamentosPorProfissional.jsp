<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Relação de Entrada e saida de Estoque Por
			Profisional</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" ng-controller="medicamentosPorProfissionalCtrl">
	<div class="col-lg-12">
		<div class="panel-group" id="accordion">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">BUSCAR HISTÓRICO</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Nome do profissional</label>
								<inputt class="form-control" type="text" name="nome" />
							</div>
						</div>
						<!-- col-lg-2 -->
						<div class="col-lg-2">
							<div class="form-group">
								<label>Data inicial</label> <inputt class="form-control" type="text" name="nome" />
							</div>
						</div>
						<!-- col-lg-2 -->
						<div class="col-lg-2">
							<div class="form-group">
								<label>Data final</label> <inputt class="form-control" type="text" name="nome" />
							</div>
						</div>
						<!-- col-lg-2 -->
						<div class="col-lg-2">
							<div class="form-group">
								<br>
								<button class="btn btn-primary" ng-click="">Buscar Histórico</button>
							</div>
						</div>
						<!-- col-lg-3 -->
					</div>
					<!-- row -->

				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Paciente: Fulano di Tal</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-2">
							<p>Relatório de Estoque</p>
							<p>Relação de entrada e saida</p>
							<p>00/00/0000 à 00/00/0000</p>
						</div>
						<!-- col-lg- -->
					</div>
					<!-- row -->

				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">FULANO DI TAL</h4>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-itensReceita">
									<thead>
										<tr>
											<th>DATA</th>
											<th>MEDICAMENTO/MATERIAL.</th>
											<th>ENTRADAS</th>
											<th>SAIDAS</th>
											<th>ESTOQUE</th>
										</tr>
									</thead>
									<tbody>
										<tr class="gradeA">
											<td>00/00/0000</td>
											<td>Dipirona Sodica 100 mg</td>
											<td>0</td>
											<td>10</td>
											<td>220</td>
										</tr>
										<tr class="gradeA">
											<td>00/00/0000</td>
											<td>Dipirona Sodica 50 mg</td>
											<td>0</td>
											<td>15</td>
											<td>210</td>
										</tr>
									</tbody>
								</table>
								<!-- /.table-responsive -->
							</div>
							<!-- /.dataTable_wrapper -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
	<script>
		$(document).ready(function() {
			$('#dataTables-buscaPacientes').DataTable({
				responsive : true
			});
		});
		$(document).ready(function() {
			$('#dataTables-itensReceita').DataTable({
				responsive : true
			});
		});
		$(document).ready(function() {
			$('#dataTables-receitasPrescritas').DataTable({
				responsive : true
			});
		});
	</script>
</div>
