<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Finalização de Receitas</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" ng-controller="finalizarReceitaCtrl">
	<div class="col-lg-12">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Buscar solicitações de receitas</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<form role="form">
							<div class="col-lg-4">
								<div class="form-group">
									<label>Nº da receita:</label>
									<input class="form-control" name="numReceita" ng-model="paciente.numReceita" placeholder="0000-000-000">
								</div>
								<div class="form-group">
									<label>Paciente:</label>
									<input class="form-control" name="nome" ng-model="paciente.nome" placeholder="Nome do paciente">
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label>Data da Emissão:</label>
									<input class="form-control" name="dtEmissao" ng-model="paciente.dtEmissao" placeholder="00/00/0000">
								</div>
							</div>
							<div class="col-lg-3">
								<div class="form-group">
									<label>Pescritor:</label>
									<input class="form-control" name="profissional" ng-model="paciente.profissional" placeholder="Nome do profissional">
								</div>
								<div class="checkbox">
									<label> 
									<input type="checkbox" ng-model="paciente.finalizada">Buscar receitas finalizadas
									</label>
								</div>
							</div>
							<div class="col-lg-3">
								<br>
								<div class="form-group">
									<button type="button" class="btn btn-success" ng-click="pesquisarReceitasAbertas(paciente)">Buscar</button>
								</div>
							</div>
						</form>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel-body">
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover"
										id="dataTables-receitasPrescritas">
										<thead>
											<tr>
												<th>Nº RECEITA</th>
												<th>PRESCRITOR</th>
												<th>STATUS</th>
												<th>DT. EMISSÃO</th>
												<th>QTDE DE ITENS</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<tr class="gradeA" ng-repeat="receita in receitas_prescritas">
												<td>{{receita.numReceita}}</td>
												<td>{{receita.profissional}}</td>
												<td><p class="btn btn-outline btn-info btn-xs">{{receita.status}}</p></td>
												<td>{{receita.dtEmissao}}</td>
												<td>{{receita.qtdeTotal}} ITENS</td>
												<td class="center">
													<p class="btn btn-outline btn-warning btn-xs">RELATÓRIO</p>
													<p class="btn btn-outline btn-success btn-xs" ng-click="buscarItensReceita(receita)">FINALIZAR</p>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- /.dataTable_wrapper -->
							</div>
							<!-- /.panel -->
						</div>
						<!-- /.col-lg-12 -->
					</div>
					<!-- /.row -->
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Itens da receita {{receitaSelecionada}}</h4>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel-body">
							<div class="dataTable_wrapper">
								
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-itensReceita">
									<thead>
										<tr>
											<th>CÓDIGO</th>
											<th>MATERIAL</th>
											<th>QTDE SOLIC.</th>
											<th>LOCAL ARMAZ.</th>
											<th><p class="btn btn-outline btn-warning btn-xs">OK?</p></th>
										</tr>
									</thead>
									<tbody>
										<tr class="gradeA" ng-repeat="item in itensReceita">
											<td>{{item.medicamento.codigo_medicamento}}</td>
											<td>{{item.medicamento.nome_medicamento}}</td>
											<td>{{item.qtdeTotal}} {{item.medicamento.unidSaida}}</td>
											<td class="center">
												<p class="btn btn-outline btn-info btn-xs">A: {{item.enderecoItem.area}}</p>
												<p class="btn btn-outline btn-primary btn-xs">R: {{item.enderecoItem.rua}}</p>
												<p class="btn btn-outline btn-danger btn-xs">M: {{item.enderecoItem.modulo}}</p>
												<p class="btn btn-outline btn-default btn-xs">N: {{item.enderecoItem.nivel}}</p>
												<p class="btn btn-outline btn-warning btn-xs">V: {{item.enderecoItem.vao}}</p>
											</td>
											<td><input type="checkbox" ng-model="item.selecionado" /></td>
										</tr>
									</tbody>
								</table>
								<!-- /.table-responsive -->
							</div>
							<!-- /.dataTable_wrapper -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.col-lg-12 -->
					<form role="form" name="itemForm2">
						<center>							
							<button type="submit" class="btn btn-primary" ng-show="itensReceita.length > 0" ng-if="opcaoSelecionada=='update'" ng-click="atualizarReceita(itensReceita)">Salva Receita</button>
						</center>
					</form>
				</div>
				<!-- /.row -->
				
				<br>
			</div>
			<div class="panel panel-default" ng-show="itensReceita.length > 0">
				<div class="well">
					<div class="row">
						<center>
							<p class="btn btn-warning btn-xs" ng-show="itensReceita.length > 0">GERAR CCOMPROVANTE DE RETIRADA</p>
						</center>
						<br>
					</div>
					<div class="row">
						<center>
							<button type="submit" class="btn btn-primary" ng-show="itensReceita.length > 0" ng-disabled="!isItemSelecionado(itensReceita)">Finalizar retirada</button>
						</center>
					</div>
					<div class="row">
						<ul>
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								Duis ipsum erat, pharetra sed finibus at, pulvinar quis dui.
								Donec vel nulla tortor.</li>
							<li>Donec suscipit tristique nibh a blandit. Aliquam eget
								nisi vitae nibh porta dictum. Ut porta, enim ac ultrices ornare,
								velit justo venenatis est, vel imperdiet eros est a sem.</li>
							<li>Vestibulum ut purus sed augue rutrum ullamcorper.
								Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
								posuere cubilia Curae</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
	<script>
		$(document).ready(function() {
			$('#dataTables-buscaPacientes').DataTable({
				responsive : true
			});
		});
		$(document).ready(function() {
			$('#dataTables-itensReceita').DataTable({
				responsive : true
			});
		});
		$(document).ready(function() {
			$('#dataTables-receitasPrescritas').DataTable({
				responsive : true
			});
		});
	</script>
</div>
