<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Movimentação</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Entrada de material</div>
			<div class="panel-body">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
					<li class="active"><a href="#movimentação" data-toggle="tab">Movimentação</a>
					</li>
					<li><a href="#entrada" data-toggle="tab">Entrada</a></li>
					<li><a href="#saida" data-toggle="tab">Saida</a></li>
					<li><a href="#itens" data-toggle="tab">Itens de Estoque</a></li>
					<li><a href="#grupo" data-toggle="tab">Grupo de itens</a></li>
					<li><a href="#cadastros" data-toggle="tab">Cadastros</a></li>
					<li><a href="#usuarios" data-toggle="tab">Usuários</a></li>
					<li><a href="#relatorios" data-toggle="tab">Relatórios</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane fade in active" id="movimentacao">
						<h4>Home Tab</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
							aute irure dolor in reprehenderit in voluptate velit esse cillum
							dolore eu fugiat nulla pariatur. Excepteur sint occaecat
							cupidatat non proident, sunt in culpa qui officia deserunt mollit
							anim id est laborum.</p>
					</div>
					<div class="tab-pane fade" id="entrada">
						<h4>Procedimento de entrada de medicamentos para estoque</h4>
						<p>Controle de entrada de medicamento para estocagem na
							farmacia</p>

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="row">
											<form role="form">
												<div class="col-lg-4">
													<div class="form-group">
														<label>Numero do documento</label> <input
															class="form-control">
														<p class="help-block">Informe o numero do Doc. de
															entrada.</p>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<br>
														<button type="submit" class="btn btn-primary">OK</button>
													</div>
												</div>

											</form>
										</div>
										<!-- /.row (nested) -->
										<div class="row">
											<div class="col-lg-12">
												<div class="panel-body">
													<div class="dataTable_wrapper">
														<table
															class="table table-striped table-bordered table-hover"
															id="dataTables-example">
															<thead>
																<tr>
																	<th>NÂº DANFE</th>
																	<th>CÃD. MED</th>
																	<th>Desc. Produo</th>
																	<th>UND ENT</th>
																	<th>UND SAI</th>
																	<th>LOTE</th>
																	<th>VALIDADE</th>
																	<th>LOCAL ARMAZ.</th>
																</tr>
															</thead>
															<tbody>
																<tr class="odd gradeX">
																	<td>000.000.300</td>
																	<td>99999</td>
																	<td>MEDICAMENTO X</td>
																	<td>CX</td>
																	<td>CT</td>
																	<td>111111</td>
																	<td>00/00/0000</td>
																	<td>A:01 R:01 M:001 N:01 V:1</td>
																</tr>
																<tr class="odd gradeX">
																	<td>000.000.300</td>
																	<td>99999</td>
																	<td>MEDICAMENTO X</td>
																	<td>CX</td>
																	<td>CT</td>
																	<td>111111</td>
																	<td>00/00/0000</td>
																	<td>A:01 R:01 M:001 N:01 V:1</td>
																</tr>
																<tr class="odd gradeX">
																	<td>000.000.300</td>
																	<td>99999</td>
																	<td>MEDICAMENTO X</td>
																	<td>CX</td>
																	<td>CT</td>
																	<td>111111</td>
																	<td>00/00/0000</td>
																	<td>A:01 R:01 M:001 N:01 V:1</td>
																</tr>
																<tr class="odd gradeX">
																	<td>000.000.300</td>
																	<td>99999</td>
																	<td>MEDICAMENTO X</td>
																	<td>CX</td>
																	<td>CT</td>
																	<td>111111</td>
																	<td>00/00/0000</td>
																	<td>A:01 R:01 M:001 N:01 V:1</td>
																</tr>
																<tr class="odd gradeX">
																	<td>000.000.300</td>
																	<td>99999</td>
																	<td>MEDICAMENTO X</td>
																	<td>CX</td>
																	<td>CT</td>
																	<td>111111</td>
																	<td>00/00/0000</td>
																	<td>A:01 R:01 M:001 N:01 V:1</td>
																</tr>
																<tr class="odd gradeX">
																	<td>000.000.300</td>
																	<td>99999</td>
																	<td>MEDICAMENTO X</td>
																	<td>CX</td>
																	<td>CT</td>
																	<td>111111</td>
																	<td>00/00/0000</td>
																	<td>A:01 R:01 M:001 N:01 V:1</td>
																</tr>
																<tr class="odd gradeX">
																	<td>000.000.300</td>
																	<td>99999</td>
																	<td>MEDICAMENTO X</td>
																	<td>CX</td>
																	<td>CT</td>
																	<td>111111</td>
																	<td>00/00/0000</td>
																	<td>A:01 R:01 M:001 N:01 V:1</td>
																</tr>
																<tr class="odd gradeX">
																	<td>000.000.300</td>
																	<td>99999</td>
																	<td>MEDICAMENTO X</td>
																	<td>CX</td>
																	<td>CT</td>
																	<td>111111</td>
																	<td>00/00/0000</td>
																	<td>A:01 R:01 M:001 N:01 V:1</td>
																</tr>
																<tr class="odd gradeX">
																	<td>000.000.300</td>
																	<td>99999</td>
																	<td>MEDICAMENTO X</td>
																	<td>CX</td>
																	<td>CT</td>
																	<td>111111</td>
																	<td>00/00/0000</td>
																	<td>A:01 R:01 M:001 N:01 V:1</td>
																</tr>
																<tr class="odd gradeX">
																	<td>000.000.300</td>
																	<td>99999</td>
																	<td>MEDICAMENTO X</td>
																	<td>CX</td>
																	<td>CT</td>
																	<td>111111</td>
																	<td>00/00/0000</td>
																	<td>A:01 R:01 M:001 N:01 V:1</td>
																</tr>
															</tbody>
														</table>
													</div>
													<!-- /.table-responsive -->
													<div class="well">
														<button type="submit" class="btn btn-primary">Gerar
															Relatorio</button>
														<button type="submit" class="btn btn-primary">Gerar
															Relatorio</button>
													</div>
												</div>
												<!-- /.panel-body -->
											</div>
											<!-- /.panel -->
											<!-- /.col-lg-12 -->
										</div>
										<!-- /.row -->
									</div>
									<!-- /.panel-body -->
								</div>
								<!-- /.panel -->
							</div>
							<!-- /.col-lg-12 -->
						</div>
						<!-- /.row -->
					</div>
					<div class="tab-pane fade" id="saida">
						<h4>Historico de Retiradas de estoque automatizado</h4>
						<p>Controle de estoque sobre as entradas e saidas de estoque</p>
						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="row">
											<div class="col-lg-12">
												<div class="panel-body">
													<div class="dataTable_wrapper">
														<table
															class="table table-striped table-bordered table-hover"
															id="dataTables-example2">
															<thead>
																<tr>
																	<th>DATA</th>
																	<th>PROTOCOLO</th>
																	<th>RETIRADAS</th>
																	<th>ALETA</th>
																	<th></th>
																</tr>
															</thead>
															<tbody>
																<tr class="gradeA">
																	<td>20/06/2016 12:15</td>
																	<td>12345667</td>
																	<td>30 UNIDS.</td>
																	<td><button type="button"
																			class="btn btn-outline btn-success btn-xs ">Em
																			conformidade</button></td>
																	<td><a href="#">relatorio</a></td>
																</tr>
																<tr class="gradeA">
																	<td>21/06/2016</td>
																	<td>32165498</td>
																	<td>30 UNIDS.</td>
																	<td><button type="button"
																			class="btn btn-outline btn-danger btn-xs ">Contigencia</button></td>
																	<td><a href="#">relatorio</a></td>
																</tr>
															</tbody>
														</table>
													</div>
													<!-- /.table-responsive -->
												</div>
												<!-- /.panel-body -->
											</div>
											<!-- /.col-lg-12 -->
										</div>
										<!-- /.row -->
									</div>
									<!-- /.panel-body -->
								</div>
								<!-- /.panel -->
							</div>
							<!-- /.col-lg-12 -->
						</div>
						<!-- /.row -->
					</div>
					<div class="tab-pane fade" id="itens">
						<h4>Settings Tab</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
							aute irure dolor in reprehenderit in voluptate velit esse cillum
							dolore eu fugiat nulla pariatur. Excepteur sint occaecat
							cupidatat non proident, sunt in culpa qui officia deserunt mollit
							anim id est laborum.</p>
					</div>
					<div class="tab-pane fade" id="grupo">
						<h4>Settings Tab</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
							aute irure dolor in reprehenderit in voluptate velit esse cillum
							dolore eu fugiat nulla pariatur. Excepteur sint occaecat
							cupidatat non proident, sunt in culpa qui officia deserunt mollit
							anim id est laborum.</p>
					</div>
					<div class="tab-pane fade" id="cadastros">
						<h4>Settings Tab</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
							aute irure dolor in reprehenderit in voluptate velit esse cillum
							dolore eu fugiat nulla pariatur. Excepteur sint occaecat
							cupidatat non proident, sunt in culpa qui officia deserunt mollit
							anim id est laborum.</p>
					</div>
					<div class="tab-pane fade" id="usuario">
						<h4>Settings Tab</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
							aute irure dolor in reprehenderit in voluptate velit esse cillum
							dolore eu fugiat nulla pariatur. Excepteur sint occaecat
							cupidatat non proident, sunt in culpa qui officia deserunt mollit
							anim id est laborum.</p>
					</div>
					<div class="tab-pane fade" id="relatorios">
						<h4>Settings Tab</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
							aute irure dolor in reprehenderit in voluptate velit esse cillum
							dolore eu fugiat nulla pariatur. Excepteur sint occaecat
							cupidatat non proident, sunt in culpa qui officia deserunt mollit
							anim id est laborum.</p>
					</div>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

