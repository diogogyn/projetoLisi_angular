<div class="row">
    <div class="col-lg-12 page-directory">
        <h6><a href="#/index">Início</a> /</h6>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Cadastro de Paciente</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row" data-ng-controller="indexCtrl">
    <
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bell fa-fw"></i> Acompanhamento de Terapia
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Javascript -->
                <form role="form">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseInclusaoAlteracao">Inclusão/Alteração</a>
                                </h4>
                            </div>
                            <div id="collapseInclusaoAlteracao" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Nome:</label>
                                                <input class="form-control" placeholder="Enter text">
                                            </div>
                                            <div class="form-group">
                                                <label>Nome Mãe:</label>
                                                <input class="form-control" placeholder="Enter text">
                                            </div>
                                            <div class="form-group">
                                                <label>Logradouro:</label>
                                                <input class="form-control" placeholder="Enter text">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Data de Nascimento:</label>
                                                <input class="form-control" placeholder="Enter text">
                                            </div> 
                                            <div class="form-group">
                                                <label>Sexo:</label>
                                                <select class="form-control">
                                                    <option>MASCULINO</option>
                                                    <option>FEMININO</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Telefone:</label>
                                                <input class="form-control" placeholder="Enter text">
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">

                                            <div class="form-group">
                                                <label>Complemento:</label>
                                                <input class="form-control" placeholder="Enter text">
                                            </div>

                                            <div class="form-group">
                                                <label>Bairro:</label>
                                                <input class="form-control" placeholder="Enter text">
                                            </div> 
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Estado:</label>
                                                <select class="form-control">
                                                    <option>MASCULINO</option>
                                                    <option>FEMININO</option>
                                                </select>
                                            </div> 
                                            <div class="form-group">
                                                <label>Cidade:</label>
                                                <input class="form-control" placeholder="Enter text">
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>CPF:</label>
                                                <input class="form-control" placeholder="Enter text">
                                            </div>
                                            <div class="form-group">
                                                <label>Situação:</label>
                                                <select class="form-control">
                                                    <option>ATIVO</option>
                                                    <option>INATIVO</option>
                                                </select>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseProntuario">Prontuário</a>
                                </h4>
                            </div>
                            <div id="collapseProntuario" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    Medicamentos Prescritos
                                                </div>
                                                <div class="panel-body">
                                                    <div class="dataTable_wrapper">
                                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                            <thead>
                                                                <tr>
                                                                    <th>Nº RECEITA</th>
                                                                    <th>PRESCRITOR</th>
                                                                    <th>STATUS</th>
                                                                    <th>DT. EMISSÃO</th>
                                                                    <th>QTDE DE ITENS</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>                                                                 
                                                                <tr class="gradeA">
                                                                    <td>2016-111-222</td>
                                                                    <td>DR. DO LITTLE</td>
                                                                    <td><p class="btn btn-outline btn-info btn-xs">aberta</p></td>
                                                                    <td>00/00/2016</td>
                                                                    <td>12 iTENS</td>
                                                                    <td class="center">
                                                                        <p class="btn btn-outline btn-warning btn-xs">RECEITA</p>
                                                                        <p class="btn btn-outline btn-success btn-xs">ALTERAR</p>
                                                                    </td>
                                                                </tr>
                                                                <tr class="gradeA">
                                                                    <td>2016-111-222</td>
                                                                    <td>DR. HOUSE</td>
                                                                    <td><p class="btn btn-outline btn-success btn-xs">FINALIZADA</p></td>
                                                                    <td>00/00/2016</td>
                                                                    <td>12 iTENS</td>
                                                                    <td class="center">
                                                                        <p class="btn btn-outline btn-warning btn-xs">RECEITA</p>
                                                                    </td>
                                                                </tr>
                                                                <tr class="gradeA">
                                                                    <td>2016-111-222</td>
                                                                    <td>DR. BELTRANO DE TAL</td>
                                                                    <td><p class="btn btn-outline btn-danger btn-xs">CANCELADA</p></td>
                                                                    <td>00/00/2016</td>
                                                                    <td>102 iTENS</td>
                                                                    <td class="center">
                                                                        <p class="btn btn-outline btn-warning btn-xs">RECEITA</p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /.table-responsive -->
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel -->
                                        </div>
                                        <!-- /.col-lg-12 -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsePlanoSaude">Plano de Saúde</a>
                                </h4>
                            </div>
                            <div id="collapsePlanoSaude" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Convênio:</label>
                                                <select class="form-control">
                                                    <option>convenio 1</option>
                                                    <option>convenio 2</option>
                                                    <option>convenio 3</option>
                                                    <option>convenio 4</option>
                                                    <option>convenio 5</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Numero do Cartão</label>
                                                <input class="form-control">
                                                <p class="help-block">Informe o numero do cartão ou do plano de saude.</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>Validade:</label>
                                                <input class="form-control">
                                                <p class="help-block">Informe a validade do plano selecionado.</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <div class="form-group">
                                                <br>
                                                <button type="submit" class="btn btn-primary">OK</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    Planos cadastrados
                                                </div>
                                                <div class="panel-body">
                                                    <div class="dataTable_wrapper">
                                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                            <thead>
                                                                <tr>
                                                                    <th>Plano de Saúde</th>
                                                                    <th>Valido até</th>
                                                                    <th>Nº cartão</th>
                                                                    <th>Ações</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="gradeA">
                                                                    <td>Sair vivo</td>
                                                                    <td>00/00/0000</td>
                                                                    <td>00.000.0-00</td>
                                                                    <td>
                                                                        <p class="btn btn-outline btn-danger btn-xs">EXCLUIR</p>
                                                                    </td>
                                                                </tr>
                                                                <tr class="gradeA">
                                                                    <td>Sair vivo</td>
                                                                    <td>00/00/0000</td>
                                                                    <td>00.000.0-00</td>
                                                                    <td>
                                                                        <p class="btn btn-outline btn-danger btn-xs">EXCLUIR</p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /.table-responsive -->
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel -->
                                        </div>
                                        <!-- /.col-lg-12 -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseAtencaoCont">Atenção Continuada</a>
                                </h4>
                            </div>
                            <div id="collapseAtencaoCont" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Atençao Continuada:</label>
                                                <select class="form-control">
                                                    <option>Problema 1</option>
                                                    <option>Problema 2</option>
                                                    <option>Problema 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <br>
                                            <button type="submit" class="btn btn-primary">OK</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    Atenções continuadas
                                                </div>
                                                <div class="dataTable_wrapper">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Código</th>
                                                                <th>Atenção Continuada</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="odd gradeX">
                                                                <td>1122</td>
                                                                <td>Internet Explorer 4.0</td>
                                                                <td>
                                                                    <p class="btn btn-outline btn-danger btn-xs">EXCLUIR</p>
                                                                </td>
                                                            </tr>
                                                            <tr class="even gradeC">
                                                                <td>2233</td>
                                                                <td>Internet Explorer 5.0</td>
                                                                <td>
                                                                    <p class="btn btn-outline btn-danger btn-xs">EXCLUIR</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                      
                    <center><button type="submit" class="btn btn-primary">Salvar</button></center>
                    
                    <br>
                </form>

            </div>
            <!-- /.panel-body-->
        </div>
        <!-- /.panel panel-default-->
    </div>
    <!-- /.col-lg-6 col-md-6-->
    <div class="col-lg-4 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bell fa-fw"></i> Notifications Panel
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="list-group">
                    <a href="#" class="list-group-item"> <i
                        class="fa fa-comment fa-fw"></i> New Comment <span
                        class="pull-right text-muted small"><em>4 minutes ago</em>
                    </span>
                    </a> <a href="#" class="list-group-item"> <i
                        class="fa fa-twitter fa-fw"></i> 3 New Followers <span
                        class="pull-right text-muted small"><em>12 minutes ago</em>
                    </span>
                    </a> <a href="#" class="list-group-item"> <i
                        class="fa fa-envelope fa-fw"></i> Message Sent <span
                        class="pull-right text-muted small"><em>27 minutes ago</em>
                    </span>
                    </a> <a href="#" class="list-group-item"> <i
                        class="fa fa-tasks fa-fw"></i> New Task <span
                        class="pull-right text-muted small"><em>43 minutes ago</em>
                    </span>
                    </a> <a href="#" class="list-group-item"> <i
                        class="fa fa-upload fa-fw"></i> Server Rebooted <span
                        class="pull-right text-muted small"><em>11:32 AM</em> </span>
                    </a> <a href="#" class="list-group-item"> <i
                        class="fa fa-bolt fa-fw"></i> Server Crashed! <span
                        class="pull-right text-muted small"><em>11:13 AM</em> </span>
                    </a> <a href="#" class="list-group-item"> <i
                        class="fa fa-warning fa-fw"></i> Server Not Responding <span
                        class="pull-right text-muted small"><em>10:57 AM</em> </span>
                    </a> <a href="#" class="list-group-item"> <i
                        class="fa fa-shopping-cart fa-fw"></i> New Order Placed <span
                        class="pull-right text-muted small"><em>9:49 AM</em> </span>
                    </a> <a href="#" class="list-group-item"> <i
                        class="fa fa-money fa-fw"></i> Payment Received <span
                        class="pull-right text-muted small"><em>Yesterday</em> </span>
                    </a>
                </div>
                <!-- /.list-group -->
                <a href="#" class="btn btn-default btn-block">View All Alerts</a>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-3 col-md-3 -->

    
</div>
<!-- /.row -->