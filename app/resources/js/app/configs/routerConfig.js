
//angular.module('angularRestfulAuth').config(function($routeProvider){
angular.module('angularRestfulAuth').config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider){

	
	console.log("passou por routConfig aqui!!!");
	
	$routeProvider
		.when('/',{
			templateUrl: '/index',
			controller: 'indexCtrl'
		})	
		.when('/error',{
			templateUrl: '/e404',
			controller: 'indexCtrl'
		})
		.when('/avisos',{
			templateUrl: '/avisos',
			controller: 'avisosCtrl'
		})
		.when("/cadastroProfissional",{
			templateUrl: '/cadastroProfissional',
			controller: 'novoProfissionalCtrl'
		})
		.when("/listaProfissionais",{
			templateUrl: '/listaProfissionais',
			controller: 'listaProfissionaisCtrl'
		})
		.when("/cadastroPaciente",{
			templateUrl: '/cadastroPaciente',
			controller: 'novoPacienteCtrl'
		})
		.when("/cadastroReceita",{
			templateUrl: '/cadastroReceita',
			controller: 'novaReceitaCtrl'
		})
		.when("/finalizarReceita",{
			templateUrl: '/finalizarReceita',
			controller: 'finalizarReceitaCtrl'
		})
		.when("/entradaMaterial",{
			templateUrl: '/entradaMaterial',
			controller: 'entradaMaterialCtrl'
		})
		.when("/middleware",{
			templateUrl: '/middleware',
			controller: 'middlewareCtrl'
		})
		.when("/medicamentosPorProfissional",{
			templateUrl: '/medicamentosPorProfissional',
			controller: 'medicamentosPorProfissionalCtrl'
		})
		.when("/comprovanteRetirada",{
			templateUrl: '/comprovanteRetirada',
			controller: 'comprovanteRetiradaCtrl'
		})
		.when("/estoqueGeral",{
			templateUrl: '/estoqueGeral',
			controller: 'estoqueGeralCtrl'
		})		
		.when("/medicamentosAVencer",{
			templateUrl: '/medicamentosAVencer',
			controller: 'medicamentosAVencerCtrl'
		})
		.when("/medicamentosPorPaciente",{
			templateUrl: '/medicamentosPorPaciente',
			controller: 'medicamentosPorPacienteCtrl'
		})
		//autuacoesPorPostoFiscalizacao
		.otherwise({redirectTo:'/error'})
}])