'use strict';

app.factory('loginService', ['$http', '$localStorage', function($http, $localStorage) {
	var baseUrl = "34.206.130.54";
	function changeUser(user) {
		angular.extend(currentUser, user);
	}

	function urlBase64Decode(str) {
		var output = str.replace('-', '+').replace('_', '/');
		switch (output.length % 4) {
		case 0:
			break;
		case 2:
			output += '==';
			break;
		case 3:
			output += '=';
			break;
		default:
			throw 'Illegal base64url string!';
		}
		return window.atob(output);
	}
	
	function getToken() {
		return $localStorage.token;;
	}
	
	function getUserFromToken() {
		var token = $localStorage.token;
		var user = {};
		if (typeof token !== 'undefined') {
			var encoded = token.split('.')[1];
			user = JSON.parse(urlBase64Decode(encoded));
		}
		return user;
	}
	
	var currentUser = getUserFromToken();

	return {
		save : function(data, success, error) {
			//url_base/api/auth
			$http.post(baseUrl + '/api/auth', data).success(success).error(error)
		},
		getToken : function() {
			return $localStorage.token;
		},
		signin : function(data, success, error) {
			$http.post(baseUrl + '/login', data).success(success).error(error)
		},
		getDadosCondominio : function(data, success, error) {
			$http.get(baseUrl + '/api/auth/'+data).success(success).error(error)
		},
		logout : function(success) {
			changeUser({});
			delete $localStorage.token;
			success();
		}
	};
} ]);
