
angular.module('angularRestfulAuth').run(function($rootScope, $location, Main) {
	$rootScope.$on('$routeChangeStart', function(event, next, current) {
		if (next.authorize) {
			if (!loginService.getToken()) {
	
				$rootScope.$evalAsync(function() {
					$location.path('/login');
				})
			}
		}
	});

});