angular.module("cadastroPostoFiscalizacao", [ "ngMessages" ]);
app.controller('cadastroPostoFiscalizacaoCtrl', function($scope) {

	$scope.enderecos = [ {
		id:1,
		nome : "Rua dos desesperados",
		quadra : 12,
		cidade : "Goiânia",
		estado : "GO",
		velocidadeVia : 40
	}, {
		id:2,
		nome : "Rua da alegria",
		quadra : 12,
		cidade : "Goiânia",
		estado : "GO",
		velocidadeVia : 100
	}, {
		id:3,
		nome : "Rua Cuca beludo",
		quadra : 12,
		cidade : "Goiânia",
		estado : "GO",
		velocidadeVia : 80
	}, {
		id:4,
		nome : "Rua panule pressão",
		quadra : 12,
		cidade : "Goiânia",
		estado : "GO",
		velocidadeVia : 60
	} ];
	$scope.init = function(){
		
		 /*CHAMA O MÉTODO consultarEnderecos DO CONTROLLER GERENCIADO PELO SPRING*/
		 var response = $http.get("consultarVias");		 
		 response.success(function(data, status, headers, config) {
			
			 /*SETA OS REGISTROS QUE FORAM RETORNADOS DO CONTROLLER DO SPRING,
			  ESSE REGISTROS VÃO PREENCHER OS CAMPOS DA TABELA DA PÁGINA consultarRegistros.jsp
			  ATAVÉS DO ng-repeat do AngularJS*/
			 $scope.vias = data;
			 
		 });		 
		 response.error(function(data, status, headers, config) {
			 /*SE OCORRER ERRO NÃO TRATADO IREMOS MOSTRA EM MENSAGEM*/
			 //$window.alert(data);
			
		 });
	 }
	
	$scope.cadastrarPostoFiscalizacao = function(){
		/*DEFINI O OBJETO QUE VAI SER ENVIADO VIA AJAX PELO ANGULARJS*/
		var postoModel =  new Object();
		postoModel.distanciaAntenas = $scope.distancia_antenas;
		postoModel.enderecoIp = $scope.endereco_ip;
		postoModel.localizacao = $scope.localizacao;
		postoModel.nomePosto = $scope.nome_posto;
		postoModel.via = $scope.via;
		/*EXECUTA O POST PARA SALVAR O REGISTRO*/
		var response = $http.post("salvarPosto", postoModel);
	 
		response.success(function(data, status, headers, config) {		
			 /*MOSTRA O RESULTADO QUE RETORNOU DO SPRING*/
			 if(data.codigo == 1){
				 
				 /*MENSAGEM DE SUCESSO*/
				 $scope.msgSuccess = data.mensagem;
				 
				 /*LIMPA OS CAMPOS APÓS SALVAR O REGISTRO COM SUCESSO*/
				 $scope.distancia_antenas = null;
				 $scope.endereco_ip = null;
				 $scope.localizacao = null;
				 $scope.nome_posto = null;
			 }else{				 
				 /*MOSTRA O ERRO TRATO PELO SPRING (OBJETO ResultadoModel)*/
				 $scope.msgSuccess = data.mensagem;				 
			 }
		 });
			
		 response.error(function(data, status, headers, config) {
			 /*SE OCORRER ERRO NÃO TRATADO IREMOS MOSTRA EM MENSAGEM*/
			 $scope.msgSuccess = data;			
		 });
	}
})