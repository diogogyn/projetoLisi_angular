
angular.module('angularRestfulAuth')
    .controller('mudancasCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterMudancas = function() {
            var formData = {
                inicio : '01/01/2017',
                 fim: '11/03/2017',
                 id : 123,
                 id_cond: 789
            }
            Main.obterMudancas(formData, function(res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.salvarMudanca = function() {
            var formData = {
                id: 021456,
                id_morador: 132,
                status: '“em análise” ou “aprovada” ou “reprovada”',
                data_solicitada:  '01/03/2017'
                //trocar pelos dados vindo do formulario (params Form)
            }
            Main.salvarMudanca(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };

    }]);
