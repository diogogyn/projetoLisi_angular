'use strict';

angular.module('angularRestfulAuth', [
    'ngStorage',
    'ngRoute'
]).config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {

    $routeProvider.
        when('/', {
            templateUrl: 'partials/home/home.html',
            controller: 'indexCtrl'
        }).
        when('/signin', {
            templateUrl: 'partials/signin.html',
            controller: 'Ctrl'
        }).
        when('/signup', {
            templateUrl: 'partials/signup.html',
            controller: 'Ctrl'
        }).
        when('/me', {
            templateUrl: 'partials/me.html',
            controller: 'Ctrl'
        }).
        when('/login', {
            templateUrl: 'partials/login/login.html',
            controller: 'loginCtrl'
        })
        .when('/condominios', {
            templateUrl: 'partials/login/condominios.html',
            controller: 'condominiosCtrl',
            authorize: true
        })
        .when('/error', {
            templateUrl: 'partials/erros/e404.html',
            controller: 'Ctrl'
        })
        .when('/autorizacao', {
            templateUrl: 'partials/autorizacao/autorizacao.hmtl',
            controller: 'autorizacoesCtrl',
            authorize: true
        })
        .when('/avisos', {
            templateUrl: 'partials/avisos/avisos.html',
            controller: 'avisosCtrl',
            authorize: true
        })
        .when('/cadastroColaborador', {
            templateUrl: 'partials/cadastro/cadastroColaborador.html',
            controller: 'cadastroColaboradoresCtrl',
            authorize: true
        })
        .when('/cadastroFornecedor', {
            templateUrl: 'partials/cadastro/cadastroFornecedor.html',
            controller: 'cadastroFornecedoresCtrl',
            authorize: true
        })
        .when("/cadastroLocalidade", {
            templateUrl: 'partials/cadastro/cadastroLocalidade.html',
            controller: 'cadastroLocalidadesCtrl',
            authorize: true
        })
        .when("/cadastroMorador", {
            templateUrl: 'partials/cadastro/cadastroMorador.html',
            controller: 'cadastroMoradoresCtrl',
            authorize: true
        })
        .when("/cadastroVeiculos", {
            templateUrl: 'partials/cadastro/cadastroVeiculos.html',
            controller: 'cadastroVeiculosCtrl',
            authorize: true
        })
        .when("/classificados", {
            templateUrl: 'partials/classificados/classificados.html',
            controller: 'classificadosCtrl',
            authorize: true
        })
        .when("/correspondencias", {
            templateUrl: 'partials/correspondencias/correspondencias.html',
            controller: 'correspondenciasCtrl',
            authorize: true
        })
        .when("/documentos", {
            templateUrl: 'partials/documentos/documentos.html',
            controller: 'documentosCtrl',
            authorize: true
        })
        .when("/mudancas", {
            templateUrl: 'partials/mudancas/mudancas.html',
            controller: 'mudancasCtrl',
            authorize: true
        })
        .when("/ocorrencias", {
            templateUrl: 'partials/ocorrencias/ocorrencias.html',
            controller: 'ocorrenciasCtrl',
            authorize: true
        })
        .when("/reservas", {
            templateUrl: 'partials/reservas/reservas.html',
            controller: 'reservasCtrl',
            authorize: true
        })
        .when("/cadastroProfissional",{
            templateUrl: '/cadastroProfissional',
            controller: 'novoProfissionalCtrl'
        })
        .when("/listaProfissionais",{
            templateUrl: '/listaProfissionais',
            controller: 'listaProfissionaisCtrl'
        })
        .when("/cadastroPaciente",{
            templateUrl: '/cadastroPaciente',
            controller: 'novoPacienteCtrl'
        })
        .when("/cadastroReceita",{
            templateUrl: '/cadastroReceita',
            controller: 'novaReceitaCtrl'
        })
        .when("/finalizarReceita",{
            templateUrl: '/finalizarReceita',
            controller: 'finalizarReceitaCtrl'
        })
        .when("/entradaMaterial",{
            templateUrl: '/entradaMaterial',
            controller: 'entradaMaterialCtrl'
        })
        .when("/middleware",{
            templateUrl: '/middleware',
            controller: 'middlewareCtrl'
        })
        .when("/medicamentosPorProfissional",{
            templateUrl: '/medicamentosPorProfissional',
            controller: 'medicamentosPorProfissionalCtrl'
        })
        .when("/comprovanteRetirada",{
            templateUrl: '/comprovanteRetirada',
            controller: 'comprovanteRetiradaCtrl'
        })
        .when("/estoqueGeral",{
            templateUrl: '/estoqueGeral',
            controller: 'estoqueGeralCtrl'
        })      
        .when("/medicamentosAVencer",{
            templateUrl: '/medicamentosAVencer',
            controller: 'medicamentosAVencerCtrl'
        })
        .when("/medicamentosPorPaciente",{
            templateUrl: '/medicamentosPorPaciente',
            controller: 'medicamentosPorPacienteCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });

    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                    } 
                    return config;
                },
                'responseError': function(response) {
                    if(response.status === 401 || response.status === 403) {
                        $location.path('/login');
                    }
                    return $q.reject(response);
                }
            };
        }]);

    }
]);