/* Controllers */

angular.module('angularRestfulAuth')
    .controller('adminUserCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterAdminUsers = function() {
            var formData = {
                id : 123
            }
            Main.obterAdminUsers(formData, function(res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.salvarAdminUser = function() {
            var formData = {
                id: 123,
                nome : 'Caliandra',
                cpf: '01524709131',
                telefone: '62984361583',
                email: 'egio@gmail.com',
                senha: '123mudar',
                type: 'root'
                //trocar pelos dados vindo do formulario (params Form)
            }
            Main.salvarAdminUser(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };
            obterCargos : function(data, success, error) {//4.9 Obter cargos
                $http.post(baseUrl + '/api/cargos', data).success(success).error(error)
            },
            salvarCargo : function(data, success, error) {//5.0 Salvar cargo
                $http.post(baseUrl + '/api/cargo', data).success(success).error(error)
            },

            obterRamosAtividade : function(data, success, error) {//5.1 Obter ramos de atividade
                $http.post(baseUrl + '/api/ramos', data).success(success).error(error)
            },
            salvarRamo : function(data, success, error) {//5.2 Salvar ramo
                $http.post(baseUrl + '/api/ramo', data).success(success).error(error)
            },

            obterPacotes : function(data, success, error) {//5.3 Obter pacote
                $http.post(baseUrl + '/api/pacotes', data).success(success).error(error)
            },
            salvarPacote : function(data, success, error) {//5.4 Salvar pacote
                $http.post(baseUrl + '/api/pacote', data).success(success).error(error)
            } 
    }]);
