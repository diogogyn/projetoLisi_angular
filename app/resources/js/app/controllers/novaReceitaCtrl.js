angular.module('angularRestfulAuth')
    .controller('novaReceitaCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

	$scope.minhaMessagem = 'oi eu estou adicionando a navegação para o site!!!';
	/*$scope.itensReceita = [];
	$scope.intervalos = [];
	$scope.nome_medicamentos = [];
	$scope.itensReceita = [];
	$scope.receitas_prescritas = [];
	$scope.pacientesBusca = [];
	$scope.pacienteSelecionado = [];//para armazenar o paciente selecionado para criar a receita
	*/
	//funcao que recebe o termo pesquisado 
	var buscarPacientes = function(nomePaciente) {
		// aqui vai o codigo do request vindo do controler
		$scope.pacientesBusca = [ {
			id : 1,
			nome : "Paciente 1",
			dtNascimento : "00/00/0000",
			nomeMae : "Fulaninha di Tal"
		}, {
			id : 2,
			nome : "Paciente 2",
			dtNascimento : "00/00/0000",
			nomeMae : "Ciclana di Tal"
		}, {
			id : 3,
			nome : "Paciente 3",
			dtNascimento : "00/00/0000",
			nomeMae : "Beltrano di Tal"
		} ];
	};
	
	var carregarMedicamentosCadastrados = function() {
		$scope.medicamentos = [ {
			codigo_medicamento : 0004,
			nome_medicamento : "Medicamento 1",
			unidSaida: "CT",
			qtde:5
		}, {
			codigo_medicamento : 0001,
			nome_medicamento : "Medicamento 2",
			unidSaida: "FR",
			qtde:3
		}, {
			codigo_medicamento : 0002,
			nome_medicamento : "Medicamento 3",
			unidSaida: "DOSE",
			qtde:2
		} ];
		$scope.intervalos = [
			{valor:1, descricao: "Por dia"},
			{valor:7, descricao: "Por semana"},
			{valor:30, descricao: "Por mês"}
		];
	};
	
	//buscar as receitas prescritas para um determinado paciente
	var carregarReceitasPrescritas = function(idPaciente) {
		$scope.receitas_prescritas = [ {
			idReceita : 1,
			numReceita : "0000-000-000",
			dtEmissao : "00/00/0000",
			dtFinalizacao : "00/00/0000",
			status : 1,
			profissional : "alguem ai"
		}, {
			idReceita : 1,
			numReceita : "0000-000-000",
			dtEmissao : "00/00/0000",
			dtFinalizacao : "00/00/0000",
			status : 1,
			profissional : "alguem ai"
		}, {
			idReceita : 1,
			numReceita : "0000-000-000",
			dtEmissao : "00/00/0000",
			dtFinalizacao : "00/00/0000",
			status : 1,
			profissional : "alguem ai"
		}, {
			idReceita : 1,
			numReceita : "0000-000-000",
			dtEmissao : "00/00/0000",
			dtFinalizacao : "00/00/0000",
			status : 1,
			profissional : "alguem ai"
		} ];
	};
	
	var carregarItensDeReceita = function(idReceita){
		$scope.itensReceita = [ {
			cod_medicamento : 0000,
			medicamento : {codigo_medicamento : 0004, nome_medicamento : "Medicamento 1", unidSaida: "CT"},
			intervalo : 1,
			qtdePrescrita : 1,
			tempoTratamento : 1,
			qtde_dispensar : 10,
			prescricao : 1 +" "+ "CX" +" p/ "+ 1 +" dia(s), por "+1 +" dia(s)"
		} ];
	};
	
	//funcao para o form para pesquisa de um paciente
	$scope.pesquisarPaciente = function(paciente){
		buscarPacientes(paciente);
		$scope.itensReceita = [];
		$scope.receitas_prescritas = [];
	};
	//para começar a registrar uma nova receita
	$scope.cadastrarNovaReceita = function(paciente){
		$scope.opcaoSelecionada = "novo";
		carregarMedicamentosCadastrados();	
		$scope.itensReceita = [];
		$scope.pacienteSelecionado = paciente;
		
	};
	$scope.pesquisarReceitasPrescritas = function(paciente){
		carregarReceitasPrescritas(paciente);
	};
	//para complementar uma receita cadastrada
	$scope.complementarReceita = function(idReceita){
		$scope.opcaoSelecionada = "update";
		carregarMedicamentosCadastrados();
		carregarItensDeReceita(idReceita);
		$scope.pacienteSelecionado = paciente;
		
	};
	
	$scope.adicionarItemReceita = function(item) {
		// colocar regra de negocio para a qtde a dispensar
		item.prescricao = item.qtdePrescrita + " " + item.medicamento.unidSaida + " p/ " + item.intervalo.valor + " dia(s), por " + item.tempoTratamento + " dia(s)"; 
		item.qtde_dispensar = (item.qtdePrescrita * item.tempoTratamento) / item.intervalo.valor;
		item.qtde_dispensar = parseInt(item.qtde_dispensar < 1 ? 1 : item.qtde_dispensar);
		$scope.minhaMessagem = item;
		$scope.itensReceita.push(angular.copy(item));
		delete $scope.item;
	};
	$scope.apagarItemReceita = function(itens) {
		$scope.itensReceita = itens.filter(function(item) {
			if (!item.selecionado)
				return item;
		});
	};
	$scope.isItemSelecionado = function(itens) {
		return itens.some(function (item) {
			return item.selecionado;
		});
	};
	$scope.isContatoSelecionado = function (contatos) {
		return contatos.some(function (contato) {
			return contato.selecionado;
		});
	};
	$scope.cadastrarReceita = function(itens,idPaciente) {

	}
	$scope.atualizarReceita = function(itens,idPaciente) {

	}
	
	
	
}])