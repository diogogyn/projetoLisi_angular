
angular.module('angularRestfulAuth')
    .controller('correspondenciasCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function ($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterCorrespondencias = function () {
            var formData = {
                id : 123,
                id_cond: 456
            };
            Main.obterCorrespondencias(formData, function (res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to load';
            });
        };

        $scope.salvarCorrespondencia = function () {
            var formData = {
                id: 123,
                data_recebimento: '01/01/2017',
                id_cond: 456,
                cpf_destinatario: '01524709131',
                data_entrega: '11/03/2017',
                nome_remetente: 'Submarino',
                nome_recebedor: 'José da Silva',
                id_tipo_pacote: 789,
                categoria: 'mercadoria',
                status: 'recebido',
                codigo_barra: '0000000000000000000000000'
                //trocar pelos dados vindo do formulario (params Form)
            };
            Main.salvarCorrespondencia(formData, function (res) {
                if (res.data.success === true) {
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to save';
            });
        };

    }]);
