
angular.module('angularRestfulAuth')
    .controller('cadastroMoradoresCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterMoradores = function () {
            var formData = {
                id : 123,
                id_cond: 456
            };
            Main.obterMoradores(formData, function (res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to load';
            });
        };

        $scope.salvarMorador = function () {
            var formData = {
                id: 021456,
                nome: 'Egio Arruda Junior',
                tipo_morador: 'locatario” ou dono',
                bloco: 'Pacari',
                apartamento: '304',
                cpf : '01524709131',
                rg : '4531240',
                email: 'egiojr@gmail.com',
                id_cond: 789,
                telefone: '62984361583'
                //trocar pelos dados vindo do formulario (params Form)
            };
            Main.salvarMorador(formData, function (res) {
                if (res.data.success === true) {
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to save';
            });
        };

    }]);
