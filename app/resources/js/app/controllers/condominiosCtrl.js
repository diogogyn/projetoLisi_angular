
angular.module('angularRestfulAuth')
    .controller('condominiosCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterCondominios = function () {
            var formData = {
                id : 123
            };
            Main.obterCondominios(formData, function (res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to load';
            });
        };

        $scope.salvarCondominio = function () {
            var formData = {
                id: 123,
                nome : 'Caliandra',
                cnpj: '11158050000118',
                logradouro: 'Rua C149',
                numero: '123',
                complemento : 'Sala 203',
                bairro: 'Jardim América',
                cep: '74740590',
                cidade: 'Goiânia',
                uf: 'GO'
                //trocar pelos dados vindo do formulario (params Form)
            };
            Main.salvarCondominio(formData, function (res) {
                if (res.data.success === true) {
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to save';
            });
        };
            
    }]);
