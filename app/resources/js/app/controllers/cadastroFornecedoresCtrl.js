
angular.module('angularRestfulAuth')
    .controller('cadastroFornecedoresCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function ($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterFornecedores = function () {
            var formData = {
                id : 123,
                id_cond: 456
            };
            Main.obterFornecedores(formData, function (res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to load';
            });
        };

        $scope.salvarFornecedor = function () {
            var formData = {
                id: 0,
                nome_fornecedor: 'Sync Consultoria',
                cpf_cnpj: '01524709131',
                id_ramo: 654,
                telefone: '62984361583'
                //trocar pelos dados vindo do formulario (params Form)
            };
            Main.salvarFornecedor(formData, function (res) {
                if (res.data.success === true) {
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to save';
            });
        };
    }]);
