/* Controllers */

angular.module('angularRestfulAuth')
    .controller('autorizacoesCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterAutorizacoes = function() {
            var formData = {
                inicio : '01/01/2017',
                fim: '11/03/2017',
                id : 123,
                id_list: 987
            }
            Main.obterAutorizacoes(formData, function(res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.obterListaAutorizacoes = function() {
            var formData = {
                inicio : '01/01/2017',
                fim: '11/03/2017',
                id : 123,
                id_cond: 465
            }
            Main.obterListaAutorizacoes(formData, function(res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.salvarAutorizacao = function() {
            var formData = {
                id: 123,
                data_autorizacao: '01/01/2017',
                id_morador: 123,
                cpf_visitante: '01524709131',
                nome_visitante: 'José da Silva',
                is_liberado: true,
                id_listagem: 987
                //trocar pelos dados vindo do formulario (params Form)
            }
            Main.salvarAutorizacao(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };
        
        $scope.salvarListaAutorizacao = function() {
            var formData = {
                id: 123,
                data_autorizacao: '01/01/2017',
                id_morador: 123,
                nome_lista: 'churrasco',
                id_local_liberado: 789,
                //trocar pelos dados vindo do formulario (params Form)
            }
            Main.salvarListaAutorizacao(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };  
    }]);
