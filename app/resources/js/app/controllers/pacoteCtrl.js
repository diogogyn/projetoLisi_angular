
angular.module('angularRestfulAuth')
    .controller('pacoteCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterPacotes = function() {
            var formData = {
                id : 123
            }
            Main.obterPacotes(formData, function(res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.salvarPacote = function() {
            var formData = {
                id: 123,
                nome : 'Sedex'
                //trocar pelos dados vindo do formulario (params Form)
            }
            Main.salvarPacote(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };

    }]);
