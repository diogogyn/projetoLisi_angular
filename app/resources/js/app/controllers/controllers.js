
angular.module('angularRestfulAuth')
    .controller('Ctrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.login = function() {
            var formData = {
                cpf: $scope.cpf,
                password: $scope.password
            }

            Main.signin(formData, function(res) {
                $localStorage.token = res.data.token;
                $location.path('/condominios');
            }, function() {
                $rootScope.error = 'Failed to signin';
            })
        };

        $scope.signup = function() {
            var formData = {
                cpf: $scope.cpf,
                password: $scope.password
            }

            Main.save(formData, function(res) {
                $localStorage.token = res.data.token;
                $location.path('/me');
            }, function() {
                $rootScope.error = 'Failed to signup';
            })
        };

        $scope.reiniciaSenha = function() {
            var formData = {
                cpf: '00000000000',
                email:'email@mail.com'
            }

            Main.reiniciaSenha(formData, function(res) {
                $scope.msg = res.data.success;
            }, function() {
                $rootScope.error = 'Failed to signup';
            })
        };

        $scope.me = function() {
            Main.me(function(res) {
                $scope.myDetails = res;
            }, function() {
                $rootScope.error = 'Failed to fetch details';
            })
        };

        $scope.logout = function() {
            Main.logout(function() {
                $location.path('/login');
            }, function() {
                $rootScope.error = 'Failed to logout';
            });
        };
    }]);
