
angular.module('angularRestfulAuth')
    .controller('ramosCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterRamosAtividade = function() {
            var formData = {
                id : 123
            }
            Main.obterRamosAtividade(formData, function(res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.salvarRamo = function() {
            var formData = {
                id: 123,
                nome : 'Tecnologia'
                //trocar pelos dados vindo do formulario (params Form)
            }
            Main.salvarRamo(formData, function(res) {
                if(res.data.success == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };

    }]);
