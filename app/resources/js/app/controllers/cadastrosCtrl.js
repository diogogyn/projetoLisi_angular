
angular.module('angularRestfulAuth')
    .controller('cadastrosCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterVeiculos = function() {
            var formData = {
                id : 123,
                id_cond: 456
            }
            Main.obterVeiculos(formData, function(res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to load';
            })
        };

        $scope.salvarVeiculo = function() {
            var formData = {
                id: 021456,
                id_morador: 123
                veiculo: 'Polo',
                marca: 'Volkswagen', 
                cor: 'Preta',
                Placa: 'NGX0001'
                //trocar pelos dados vindo do formulario (params Form)
            }
            Main.salvarVeiculo(formData, function(res) {
                if(res.data == true){
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function() {
                $rootScope.error = 'Failed to save';
            })
        };
    }]);
