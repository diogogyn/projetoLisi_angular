
angular.module('angularRestfulAuth')
    .controller('documentosCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterDocumentos = function () {
            var formData = {
                id : 123,
                id_cond: 564
            };
            Main.obterDocumentos(formData, function (res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to load';
            });
        };

        $scope.salvarDocumentos = function () {
            var formData = {
                id: 123,
                id_cond: 987,
                nome: 'ata reunião',
                link: '#'
                //trocar pelos dados vindo do formulario (params Form)
            };
            Main.salvarDocumentos(formData, function (res) {
                if (res.data.success === true) {
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to save';
            });
        };

    }]);
