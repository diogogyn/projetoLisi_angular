angular.module("gerenciaPostoFiscalizacao", ["ngMessages"]);
app.controller('gerenciaPostoFiscalizacaoCtrl', function($scope) {
	console.log("passou por gerenciaPostoFiscalizacao");
	
	$scope.ligarPostofiscalizacao = function(posto){
		
		/*EXECUTA O POST PARA ALTERAR O REGISTRO*/
		var response = $http.post("#/inicializarPosto", posto);
		
		response.success(function(data, status, headers, config) {
	
		 /*MOSTRA O RESULTADO QUE RETORNOU DO SPRING*/
		 if(data.codigo == 1){			 
			 /*MENSAGEM DE SUCESSO*/
			 $msgServidor = data.mensagem;
		 }
		 else{			 
			 /*MOSTRA O ERRO TRATO PELO SPRING (OBJETO ResultadoModel)*/
			 $msgServidor = data.mensagem;			 
		 }
	 });
		
	 response.error(function(data, status, headers, config) {
		 /*SE OCORRER ERRO NÃO TRATADO IREMOS MOSTRA EM MENSAGEM*/
		 $window.alert(data);		
	 });					
		
	};
	$scope.desligarPostofiscalizacao = function(posto){
		
		/*EXECUTA O POST PARA ALTERAR O REGISTRO*/
		var response = $http.post("#/interromperPosto", posto);
		
		response.success(function(data, status, headers, config) {
	
		 /*MOSTRA O RESULTADO QUE RETORNOU DO SPRING*/
		 if(data.codigo == 1){			 
			 /*MENSAGEM DE SUCESSO*/
			 $msgServidor = data.mensagem;			 
		 }
		 else{			 
			 /*MOSTRA O ERRO TRATO PELO SPRING (OBJETO ResultadoModel)*/
			 $msgServidor = data.mensagem;			 
		 }
	 });
		
	 response.error(function(data, status, headers, config) {
		 /*SE OCORRER ERRO NÃO TRATADO IREMOS MOSTRA EM MENSAGEM*/
		 $window.alert(data);		
	 });		
				
		
	};
})