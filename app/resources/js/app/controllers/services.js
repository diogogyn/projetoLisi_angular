angular.module('angularRestfulAuth')
    .factory('Main', ['$http', '$localStorage', function ($http, $localStorage) {
        var baseUrl = "34.206.130.54";
        function changeUser(user) {
            angular.extend(currentUser, user);
        }

        function urlBase64Decode(str) {
            var output = str.replace('-', '+').replace('_', '/');
            switch (output.length % 4) {
                case 0:
                    break;
                case 2:
                    output += '==';
                    break;
                case 3:
                    output += '=';
                    break;
                default:
                    throw 'Illegal base64url string!';
            }
            return window.atob(output);
        }

        function getUserFromToken() {
            var token = $localStorage.token;
            var user = {};
            if (typeof token !== 'undefined') {
                var encoded = token.split('.')[1];
                user = JSON.parse(urlBase64Decode(encoded));
            }
            return user;
        }

        var currentUser = getUserFromToken();

        return {
            save : function (data, success, error) {
                $http.post(baseUrl + '/api/auth', data).success(success).error(error);
            },
            getToken : function () {
                return $localStorage.token;
            },
            login : function (data, success, error) {//1.1 Login
                $http.post(baseUrl + '/api/auth', data).success(success).error(error);
            },
            selecionaCondominio : function (data, success, error) {//1.1.1 Seleciona Condomínio
                $http.get(baseUrl + '/api/auth/' + data).success(success).error(error);
            },
            reiniciaSenha : function (data, success, error) {//1.1.2 Reinicia senha
                $http.post(baseUrl + '/api/user/reset_password', data).success(success).error(error);
            },
            logout: function (success) {//1.2 Logout
                changeUser({});
                delete $localStorage.token;
                success();
            },
            obterVeiculos : function (data, success, error) {//2.1 Obter Veículos
                $http.post(baseUrl + '/api/veiculos', data).success(success).error(error);
            },
            salvarVeiculo : function (data, success, error) {//2.2 Salvar Veículo
                $http.post(baseUrl + '/api/veiculo', data).success(success).error(error);
            },
            obterMoradores : function (data, success, error) {//2.3 Obter Moradores
                $http.post(baseUrl + '/api/moradores', data).success(success).error(error);
            },
            salvarMorador : function (data, success, error) {//2.4 Salvar Morador
                $http.post(baseUrl + '/api/morador', data).success(success).error(error);
            },
            obterColaboradores : function (data, success, error) {//2.3 Obter Colaboradores
                $http.post(baseUrl + '/api/colaboradores', data).success(success).error(error);
            },
            salvarColaborador : function (data, success, error) {//2.4 Salvar Veículo
                $http.post(baseUrl + '/api/colaborador', data).success(success).error(error);
            },
            obterFornecedores : function (data, success, error) {//2.5 Obter Fornecedores
                $http.post(baseUrl + '/api/fornecedores', data).success(success).error(error);
            },
            salvarFornecedor : function (data, success, error) {//2.6 Salvar Fornecedor
                $http.post(baseUrl + '/api/fornecedor', data).success(success).error(error);
            },

            obterLocalidades : function (data, success, error) {//2.7 Obter Localidades
                $http.post(baseUrl + '/api/localidades', data).success(success).error(error);
            },
            salvarLocalidade : function (data, success, error) {//2.7 Salvar Localidade
                $http.post(baseUrl + '/api/localidade', data).success(success).error(error);
            },

            obterOcorrencias : function (data, success, error) {//2.9 Obter Ocorrencias
                $http.post(baseUrl + '/api/ocorrencias', data).success(success).error(error);
            },
            salvarOcorrencia : function (data, success, error) {//3.0 Salvar Ocorrencia
                $http.post(baseUrl + '/api/ocorrencia', data).success(success).error(error);
            },

            agruparOcorrencias : function (data, success, error) {//3.1 Agrupar ocorrências
                $http.post(baseUrl + '/api/ocorrência/agrupar', data).success(success).error(error);
            },
            salvarMensagemOcorrencia : function (data, success, error) {//3.2 Salvar mensagem na ocorrencia
                $http.post(baseUrl + '/api/ocorrencia/mensagem', data).success(success).error(error);
            },

            obterAvisos : function (data, success, error) {//3.3 Obter Avisos
                $http.post(baseUrl + '/api/avisos', data).success(success).error(error);
            },
            salvarAviso : function (data, success, error) {//3.4 Salvar Aviso
                $http.post(baseUrl + '/api/aviso', data).success(success).error(error);
            },

            obterReservas : function (data, success, error) {//3.5 Obter Reservas
                $http.post(baseUrl + '/api/reservas', data).success(success).error(error);
            },
            salvarReserva : function (data, success, error) {//3.6 Salvar Reserva
                $http.post(baseUrl + '/api/reserva', data).success(success).error(error);
            },

            obterMudancas : function (data, success, error) {//3.7 Obter Mudanças
                $http.post(baseUrl + '/api/mudancas', data).success(success).error(error);
            },
            salvarMudanca : function (data, success, error) {//3.8 Salvar Mudança
                $http.post(baseUrl + '/api/mudanca', data).success(success).error(error);
            },

            obterCorrespondencias : function (data, success, error) {//3.9 Obter Correspondências
                $http.post(baseUrl + '/api/correspondencias', data).success(success).error(error);
            },
            salvarCorrespondencia : function (data, success, error) {//4.0 Salvar Correspondência
                $http.post(baseUrl + '/api/correspondencia', data).success(success).error(error);
            },

            obterAutorizacoes : function (data, success, error) {//4.1 Obter Autorizações
                $http.post(baseUrl + '/api/autorizacoes', data).success(success).error(error);
            },
            obterListaAutorizacoes : function (data, success, error) {//4.1.1 Obter Lista de Autorizações
                $http.post(baseUrl + '/api/listas_autorizacao', data).success(success).error(error);
            },
            salvarAutorizacao : function (data, success, error) {//4.2 Salvar Autorização
                $http.post(baseUrl + '/api/autorizacao', data).success(success).error(error);
            },
            salvarListaAutorizacao : function (data, success, error) {//4.2.1 Salvar Lista Autorização
                $http.post(baseUrl + '/api/lista_autorizacao', data).success(success).error(error);
            },

            obterDocumentos : function (data, success, error) {//4.1 Obter Documentos
                $http.get(baseUrl + '/api/documentos', data).success(success).error(error);
            },
            salvarDocumentos : function (data, success, error) {//4.2 Salvar Documentos
                $http.post(baseUrl + '/api/Documentos', data).success(success).error(error);
            },

            obterClassificados : function (data, success, error) {//4.3 Obter Classificados
                $http.post(baseUrl + '/api/classificados', data).success(success).error(error);
            },
            salvarClassificado : function (data, success, error) {//4.4 Salvar Classificado
                $http.post(baseUrl + '/api/classificado', data).success(success).error(error);
            },

            obterCondominios : function (data, success, error) {//4.5 Obter Condominios
                $http.post(baseUrl + '/api/condominios', data).success(success).error(error);
            },
            salvarCondominio : function (data, success, error) {//4.6 Salvar Condomínio
                $http.post(baseUrl + '/api/condominio', data).success(success).error(error);
            },

            obterAdminUsers: function (data, success, error) {//4.7 Obter admin users
                $http.post(baseUrl + '/api/admin/users', data).success(success).error(error);
            },
            salvarAdminUser : function (data, success, error) {//4.8 Salvar admin user
                $http.post(baseUrl + '/api/admin/user', data).success(success).error(error);
            },

            obterCargos : function (data, success, error) {//4.9 Obter cargos
                $http.post(baseUrl + '/api/cargos', data).success(success).error(error);
            },
            salvarCargo : function (data, success, error) {//5.0 Salvar cargo
                $http.post(baseUrl + '/api/cargo', data).success(success).error(error);
            },

            obterRamosAtividade : function (data, success, error) {//5.1 Obter ramos de atividade
                $http.post(baseUrl + '/api/ramos', data).success(success).error(error);
            },
            salvarRamo : function (data, success, error) {//5.2 Salvar ramo
                $http.post(baseUrl + '/api/ramo', data).success(success).error(error);
            },

            obterPacotes : function (data, success, error) {//5.3 Obter pacote
                $http.post(baseUrl + '/api/pacotes', data).success(success).error(error);
            },
            salvarPacote : function (data, success, error) {//5.4 Salvar pacote
                $http.post(baseUrl + '/api/pacote', data).success(success).error(error);
            }
        };
    }
        ]);
