angular.module('angularRestfulAuth')
    .controller('classificadosCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        $scope.obterClassificados = function () {
            var formData = {
                id : 123,
                id_cond: 564
            };
            Main.obterClassificados(formData, function (res) {
                $scope.dados = res.data.success;
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to load';
            });
        };

        $scope.salvarClassificado = function () {
            var formData = {
                id: 123,
                image_ulr: '#',
                titulo: 'Fast Açaí Promoção',
                id_cond: 321,
                descricao: 'nononononononononononononononononononon',
                nome_responsavel: 'Douglas Jason',
                numero: '62984361583'
                //trocar pelos dados vindo do formulario (params Form)
            };
            Main.salvarClassificado(formData, function (res) {
                if (res.data.success === true) {
                    $scope.msg = 'Dados salvos com sucesso';
                }
                //implementar outras saidas e comandos
            }, function () {
                $rootScope.error = 'Failed to save';
            });
        };
    }]);
