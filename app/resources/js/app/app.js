'use strict';

angular.module('angularRestfulAuth', [
    'ngStorage',
    'ngRoute'
]).config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {

    $routeProvider.
        when('/', {
            templateUrl: '/partials/home/home.html',
            controller: 'indexCtrl'
        }).
        when('/index', {
            templateUrl: 'partials/home/home.html',
            controller: 'indexCtrl'
        }).
        when('/login', {
            templateUrl: 'partials/login/login.html',
            controller: 'loginCtrl'
        })        
        .when('/error', {
            templateUrl: 'partials/erros/e404.html',
            controller: 'indexCtrl'
        })
        .when('/avisos', {
            templateUrl: 'partials/avisos/avisos.html',
            controller: 'avisosCtrl',
            authorize: true
        })
        .when("/documentos", {
            templateUrl: 'partials/documentos/documentos.html',
            controller: 'documentosCtrl',
            authorize: true
        })
        .when("/mudancas", {
            templateUrl: 'partials/mudancas/mudancas.html',
            controller: 'mudancasCtrl',
            authorize: true
        })
        .when("/ocorrencias", {
            templateUrl: 'partials/ocorrencias/ocorrencias.html',
            controller: 'ocorrenciasCtrl',
            authorize: true
        })
        .when("/reservas", {
            templateUrl: 'partials/reservas/reservas.html',
            controller: 'reservasCtrl',
            authorize: true
        })
        .when("/cadastroProfissional",{
            templateUrl: '/cadastroProfissional',
            controller: 'novoProfissionalCtrl'
        })
        .when("/listaProfissionais",{
            templateUrl: '/listaProfissionais',
            controller: 'listaProfissionaisCtrl'
        })
        .when("/cadastroPaciente",{
            templateUrl: '/cadastroPaciente',
            controller: 'novoPacienteCtrl'
        })
        .when("/cadastroReceita",{
            templateUrl: '/cadastroReceita',
            controller: 'novaReceitaCtrl'
        })
        .when("/finalizarReceita",{
            templateUrl: '/finalizarReceita',
            controller: 'finalizarReceitaCtrl'
        })
        .when("/entradaMaterial",{
            templateUrl: '/entradaMaterial',
            controller: 'entradaMaterialCtrl'
        })
        .when("/middleware",{
            templateUrl: '/middleware',
            controller: 'middlewareCtrl'
        })
        .when("/medicamentosPorProfissional",{
            templateUrl: '/medicamentosPorProfissional',
            controller: 'medicamentosPorProfissionalCtrl'
        })
        .when("/comprovanteRetirada",{
            templateUrl: '/comprovanteRetirada',
            controller: 'comprovanteRetiradaCtrl'
        })
        .when("/estoqueGeral",{
            templateUrl: '/estoqueGeral',
            controller: 'estoqueGeralCtrl'
        })      
        .when("/medicamentosAVencer",{
            templateUrl: '/medicamentosAVencer',
            controller: 'medicamentosAVencerCtrl'
        })
        .when("/medicamentosPorPaciente",{
            templateUrl: '/medicamentosPorPaciente',
            controller: 'medicamentosPorPacienteCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });

   /* $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                    } 
                    return config;
                },
                'responseError': function(response) {
                    if(response.status === 401 || response.status === 403) {
                        $location.path('/login');
                    }
                    return $q.reject(response);
                }
            };
        }]);

    }*/
]);

/*'use strict';
var dependencias = [ 'ngRoute'
                    ,'ngResource',
                    'ngStorage'];

var app = angular.module('app',dependencias)
.config(['$httpProvider', function ($routeProvider, $httpProvider) {

    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function($q, $location, $localStorage) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                    }
                    return config;
                },
                'responseError': function(response) {
                    if(response.status === 401 || response.status === 403) {
                        $location.path('/login');
                    }
                    return $q.reject(response);
                }
            };
        }]);

    }
]);*/
